import 'package:flutter/material.dart';
import 'package:simple_pong/Pong.dart';

void main() => runApp(MyApp());

String appTitle = "Pong Demo";

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: appTitle,
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text(appTitle),
        ),
        body: SafeArea(
          child: Pong(),
        ),
      ),
    );
  }
}
